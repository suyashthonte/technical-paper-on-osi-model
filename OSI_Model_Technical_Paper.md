# OSI Model

**OSI** stands for **O**pen **S**ystem **I**nterconnection Model. OSI defines how data is transferred from one computer to another over a computer network. So if 2 computers of different configurations or operating systems were to connect to each other, how would the data transfer occur? This is what exactly OSI Model describes!


OSI Model was developed by ISO and it consists of the following 7 layers:
1. Application Layer
2. Presentation Layer
3. Session Layer
4. Transport Layer
5. Network Layer
6. Data Link Layer
7. Physical Layer

>Here every Layer is a set of Protocols.

##  1. Application Layer :
Application layer is used by network applications (like Chrome, Firefix, Skype, Gmail, etc). So these network applications use application layer protocols like ***HTTP or HTTPS*** for web surfing, ***FTP*** for file transfer, ***SMTP*** for emails and ***TELNET*** for virtual machines. So Application Layer provides services for network applications with the help of these protocols.

## 2. Presentation Layer :
Presentation Layer converts the data(characters and numbers) recieved from application layer to machine-readable binary format. This conversion process is called as ***Translation***. After translation, presentation layer compresses the data by reducing the bits and this process is called ***Data Compression*** (resulting in faster transfer rates).Later the presentation layer provides ***Data Encryption*** to data before it is transmitted. SSL Protocol is used by presentation layer for encryption and decryption of data.

## 3. Session Layer :
Session Layer is responsible for hoisting the connections and managing them, therefore enabling the transmission of data as well as terminating the the hoisted sessions. Session layer uses ***API's (Application Programming Interface)*** for establishing the connections. Session layer implements ***Authentication, Authorization and Session Management*** during a session.

## 4. Transport Layer :
Transport Layer implements effective and reliable communication through ***Segmentation, Flow Control and Error Control***. In ***Segementation*** recieved data is divided into sub packets called segments which contain : *source port number, destination port number and sequence number.* In ***flow control***, transport layer limits the amount of data transmission from server to client and vice-versa in order to limit data loss as well as maintain system performance. In ***error control***, transport layer uses ARQ (Automatic Repeat Request) schemes to retransmit lost or corrupted data. 
Transport Layer has 2 protocols :
* ***Transmission Control Protocol (TCP)*** - Connection oriented : TCP occurs when session is established.
* ***User Datagram Protocol (UDP)*** - Connectionless : UDP occurs without a session or predefined path.

## 5. Network Layer :
Network Layer is responsible for transfer of these recieved segments (from transport layer) from one computer to other in a network. This transfer occurs in small units called ***Packets***. Network Layer performs these functions :
* ***Logical Addressing :*** Network Layer assigns IP addresses to segments to ensure that sender and reciever both communicate with the defined and correct data segments. This Logical IP addressing is based on IPv4 and IPv6 formats along with the mask.
* ***Path Determination :*** Network layer chooses the best possible path from the available paths so that the data transmission between the source and destination is the fastest, this is called as Path Determination. Network Layer uses protocols such as OSPF(Open Shortest Path First) or BGP(Border Gateway Protocol) for Path Determination.

## 6. Data Link Layer :
Data Link Layer recieves the data from Network Layer in the form of IP adresses and it implements ***Physical Addressing*** on these IP addresses.
In this the Data Link Layer ***MAC*** adresses of sender and reciever are assigned to the recieved data packets and this whole new data packet is called as Frame. These frames are read by Data Link Layer which exists as a software in ***Network Interface Card(NIC)*** of every computer and they provide a medium to transfer data locally via Physical Layer.

## 7. Physical Layer :
The obtained data from the above layers is in binary format which gets converted to various signals such as :
***-- electrical signals in case of copper wire or LAN.***
***-- light signals in case of optical fibre.***
***-- radio signals in case of air***
by physical layer.

After this the recieved data is propogated to higher layers i.e from physical layer to application layer by converting it or decapsulating it to its respective forms via protocols and thus displayed to user via application layer.

In this way, the ***7 layers of OSI Model*** work in background to ensure smooth functioning of data communication between two remote comopters.


References : 
[Reference 1](https://www.javatpoint.com/osi-model)
[Reference 2](https://www.youtube.com/watch?v=hpAJMSS8pvs&feature=youtu.be)